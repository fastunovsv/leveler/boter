package interceptor

import (
	"context"

	"github.com/getsentry/sentry-go"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func SentryUnaryServerInterceptor() grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
		defer func() {
			if e := recover(); e != nil {
				sentry.CurrentHub().Recover(e)
				err = status.Error(codes.Internal, "internal server error")
			}
		}()

		resp, err = func() (interface{}, error) {
			hub := sentry.CurrentHub().Clone()
			hub.Scope().SetExtra("requestBody", req)

			resp, err := handler(ctx, req)
			if err != nil {
				_ = hub.CaptureException(err)
			}

			return resp, err
		}()

		return resp, err
	}
}
