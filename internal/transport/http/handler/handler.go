package handler

import (
	"context"
	"net/http"

	"github.com/friendsofgo/errors"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"google.golang.org/grpc/status"
)

func RoutingErrorHandle(ctx context.Context, mux *runtime.ServeMux, marshaler runtime.Marshaler, w http.ResponseWriter, r *http.Request, e error) {
	s, ok := status.FromError(e)
	if !ok {
		code := http.StatusInternalServerError
		err := &runtime.HTTPStatusError{
			HTTPStatus: code,
			Err:        errors.New(http.StatusText(code)),
		}

		runtime.DefaultHTTPErrorHandler(ctx, mux, marshaler, w, r, err)
	}

	// NOTE: место для обработки кастомной ошбки
	// Лучше всегдо сопостовлять http код ошибки по сообщения из ошибки (s.Message())

	code := runtime.HTTPStatusFromCode(s.Code())
	err := &runtime.HTTPStatusError{
		HTTPStatus: code,
		Err:        s.Err(),
	}

	runtime.DefaultHTTPErrorHandler(ctx, mux, marshaler, w, r, err)
}
