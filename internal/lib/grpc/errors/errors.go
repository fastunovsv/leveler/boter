package errors

const (
	ErrGetData          = "grpc get data error"
	ErrInvalidArgument  = "invalid argument"
	ErrInternalServer   = "internal server error"
	ErrNotExist         = "not exist"
	ErrPermissionDenied = "permission denied"
)
