package dto

import (
	"time"
)

type Survey struct {
	ID          string
	Name        string
	Description string
	Enable      *bool
}

type Review struct {
	ID            string
	Name          string
	ReviewStart   *time.Time
	SetupDate     *time.Time
	ProcessStart  *time.Time
	ApprovalStart *time.Time
	SummaryDate   *time.Time
	ReviewEnd     *time.Time
	Status        ReviewStatus
	Surveys       []Survey
}

type ReviewStatus struct {
	ID         string
	Value      string
	StatusType string
}
