package config

import (
	"fmt"
	"gopkg.in/yaml.v3"
	"os"
	"path/filepath"
)

type Config struct {
	App struct {
		Environment string `yaml:"environment"`
		Key         string `yaml:"key"`
		LogLevel    string `yaml:"log-level"`
		GRPC        struct {
			Host string `yaml:"host"`
			Port string `yaml:"port"`
		} `yaml:"grpc"`
		HTTP struct {
			Host string `yaml:"host"`
			Port string `yaml:"port"`
		} `yaml:"http"`
	} `yaml:"app"`
	Database struct {
		Host     string `yaml:"host"`
		Port     uint16 `yaml:"port"`
		Database string `yaml:"database"`
		User     string `yaml:"user"`
		Password string `yaml:"password"`
		SSLmode  string `yaml:"sslmode"`
	} `yaml:"database"`
	Clients struct {
		HRGateway struct {
			Host string `yaml:"host"`
			Port string `yaml:"port"`
		} `yaml:"hr-gateway"`
		IDPService struct {
			Host string `yaml:"host"`
			Port string `yaml:"port"`
		} `yaml:"idp-service"`
	} `yaml:"clients"`
	Sentry struct {
		DSN string `yaml:"dsn"`
	} `yaml:"sentry"`
}

func NewConfig(configPath string) (config Config, err error) {
	filename, _ := filepath.Abs(configPath)
	yamlFile, err := os.ReadFile(filename)
	if err != nil {
		return config, err
	}
	err = yaml.Unmarshal(yamlFile, &config)
	if err != nil {
		return
	}

	if config.App.LogLevel == "" {
		config.App.LogLevel = "Error"
	}

	return
}

func (c *Config) DatabaseUrl() string {
	return fmt.Sprintf(
		"postgres://%s:%s@%s:%d/%s?sslmode=%s",
		c.Database.User, c.Database.Password,
		c.Database.Host, c.Database.Port,
		c.Database.Database,
		c.Database.SSLmode,
	)
}
